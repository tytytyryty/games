﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour {

    public float shotTimeout;
    public float shotEffectTime;
    public ParticleSystem shootParticle;
    
    float lastShotTime = 0;
    LineRenderer shootLine;
    Light shootLight;
    int shootableMask;
    Ray shootRay;
    RaycastHit shootHit;
    float shootRange = 100f;
    int damagePerShot = 10;
    AudioSource shootAudio;

    void Awake()
    {
        shootLine = GetComponent<LineRenderer>();
        shootLight = GetComponent<Light>();
        shootAudio = GetComponent<AudioSource>();
        shootableMask = LayerMask.GetMask("Shootable");
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && Time.timeSinceLevelLoad > lastShotTime + shotTimeout)
        {
            Shoot();
        }

        if (Time.timeSinceLevelLoad > lastShotTime + shotEffectTime)
        {
            DisableShotEffects();
        }
    }

    private void Shoot()
    {
        lastShotTime = Time.timeSinceLevelLoad;

        shootParticle.Play();
        shootLight.enabled = true;
        shootLine.enabled = true;
        shootLine.SetPosition(0, transform.position);
        shootAudio.Play();

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;
            
        if (Physics.Raycast(shootRay, out shootHit, shootRange, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
                
            shootLine.SetPosition(1, shootHit.point);
        }
        else
        {
            shootLine.SetPosition(1, shootRay.origin + shootRay.direction * shootRange);
        }
    }

    private void DisableShotEffects()
    {
        shootLight.enabled = false;
        shootLine.enabled = false;
    }
}
