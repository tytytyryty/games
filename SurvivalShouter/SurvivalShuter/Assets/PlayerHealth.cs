﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int currentHealth = 100;
    public Image healthImage;
    public Image hitImage;
    public float flashSpeed = 5f;                               
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);    

    Animator animator;

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Update()
    {
        hitImage.color = Color.Lerp(hitImage.color, Color.clear, flashSpeed * Time.deltaTime);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        UpdateHealthImage();
        hitImage.color = flashColour;
        if (currentHealth <= 0)
        {
            animator.SetTrigger("Died");
        }
    }

    public bool IsDead
    {
        get { return currentHealth <= 0; }
    }

    private void UpdateHealthImage()
    {
        var color = healthImage.color;
        color.a = currentHealth / 100f;
        healthImage.color = color;
    }
}
