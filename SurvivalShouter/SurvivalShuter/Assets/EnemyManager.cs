﻿using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public Transform enemy;
    public int spawnDelay;
    
	void Awake()
    {
        InvokeRepeating("Spawn", spawnDelay, spawnDelay);
	}
	
	void Spawn()
    {
        Instantiate(enemy, enemy.position, enemy.rotation);
	}
}
