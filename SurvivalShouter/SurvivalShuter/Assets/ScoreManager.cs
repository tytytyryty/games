﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public Text scoreText;
    private int score = 0;
    
    public void Awake()
    {

    }

    public void AppendScore(int scorePoints)
    {
        score += scorePoints;
        scoreText.text = "Score: " + score;
    }
}
