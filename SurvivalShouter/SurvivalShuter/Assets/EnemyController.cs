﻿using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int damage = 10;
    public float attackTimeout = 0.5f;
    
    GameObject player;
    PlayerHealth playerHealth;
    NavMeshAgent navAgent;
    Animator animator;
    EnemyHealth health;
    bool playerInRange;
    float lastAttackTime = 0f;

    void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        navAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        health = GetComponent<EnemyHealth>();
    }
	
	void Update () {
        if (!playerHealth.IsDead && health.currentHealth > 0)
        {
            Follow();
            TryAttack();
        }
	}
    
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerInRange = false;
        }
    }
    
    private void Follow()
    {
        navAgent.SetDestination(player.transform.position);
    }

    private void TryAttack()
    {
        if (playerInRange && lastAttackTime + attackTimeout < Time.time)
        {
            lastAttackTime = Time.time;
            playerHealth.TakeDamage(damage);
            if (playerHealth.IsDead)
            {
                animator.SetTrigger("PlayerDied");
                navAgent.enabled = false;
            }
        }
    }
}
