﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public Transform player;

    Vector3 offset;
    public float smoothing = 5f;

    void Start () {
        offset = transform.position - player.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        var targetCameraPosition = player.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCameraPosition, smoothing * Time.deltaTime);
    }
}
