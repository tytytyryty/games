﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float rotationSpeed;

    Rigidbody rb;
    Animator animator;
    float camRayLength = 100f; // The length of the ray from the camera into the scene
    int floorMask;
    PlayerHealth health;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        health = GetComponent<PlayerHealth>();
        floorMask = LayerMask.GetMask("Floor");
    }
	
	void FixedUpdate ()
    {
        if (!health.IsDead)
        {
            var h = Input.GetAxisRaw("Horizontal");
            var v = Input.GetAxisRaw("Vertical");

            Move(h, v);

            Turn();

            Animate(h, v);
        }
    }
    
    private void Move(float h, float v)
    {
        var movement = new Vector3(h, 0, v).normalized * speed * Time.deltaTime;
        rb.MovePosition(rb.position + movement);
    }

    private void Turn()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit floorHit;

        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            rb.MoveRotation(newRotation);
        }
    }

    private void Animate(float h, float v)
    {
        bool isWalking = h != 0f || v != 0f;
        animator.SetBool("IsWalking", isWalking);
    }
}
