﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public int currentHealth = 50;
    public float sinkSpeed = 2f;
    public int scorePoints = 10;

    bool IsDead = false;
    bool isSinking = false;
    Animator animator;
    NavMeshAgent navAgent;
    CapsuleCollider capsuleCollider;
    ParticleSystem hitParticle;
    ScoreManager scoreManager;

    void Awake()
    {
        animator = GetComponent<Animator>();
        navAgent = GetComponent<NavMeshAgent>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        hitParticle = GetComponentInChildren<ParticleSystem>();
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
    }
	
    void Update()
    {
        if (isSinking)
        {
            transform.Translate(Vector3.down * sinkSpeed * Time.deltaTime);
            //transform.position = Vector3.Lerp(transform.position, Vector3.down, sinkSpeed * Time.deltaTime);
        }
    }

    public void TakeDamage(int damage, Vector3 hitPoint)
    {
        if (!IsDead)
        {
            currentHealth -= damage;
            hitParticle.transform.position = hitPoint;
            hitParticle.Play();

            if (currentHealth <= 0)
            {
                IsDead = true;
                scoreManager.AppendScore(scorePoints);
                navAgent.enabled = false;
                capsuleCollider.isTrigger = true; //bullets can pass through it nows
                animator.SetTrigger("Died");
            }
        }
    }

    public void StartSinking()
    {
        isSinking = true;
        
        Destroy(gameObject, 2f);
    }
}
