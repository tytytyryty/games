﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {

    public float restartDelay = 5f;
    public float gameOverScreenDelay = 3f;
    public PlayerHealth playerHealth;

    float restartTimer;
    
    void Awake()
    {
        
    }
	
	void Update ()
    {
        if (playerHealth.currentHealth <= 0)
        {
            StartCoroutine(GameOver());
        }
    }

    IEnumerator GameOver()
    {
        yield return new WaitForSeconds(gameOverScreenDelay);

        GetComponent<Animator>().SetTrigger("GameOver");

        restartTimer += Time.deltaTime;
        if (restartTimer >= restartDelay)
        {
            SceneManager.LoadScene("main");
        }
    }
}
