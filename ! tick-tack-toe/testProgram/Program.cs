﻿using System;
using tick_tack_toe.GameLogic;
using tick_tack_toe.GameLogic.Enums;

namespace testProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            var position = new Position(new[]
		    {
			    Cell.X,     Cell.Empty, Cell.O,
			    Cell.Empty, Cell.Empty, Cell.X, 
			    Cell.Empty, Cell.Empty, Cell.Empty
		    });
            var move = Element.O;
            var depth = 2;

            new AI().GetBestMoveIndex(position, move, depth);
            Console.ReadLine();
        }
    }
}
