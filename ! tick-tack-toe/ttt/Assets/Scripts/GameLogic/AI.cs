﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tick_tack_toe.GameLogic.Enums;

namespace tick_tack_toe.GameLogic
{
    public class AI
    {
        private int _bestMoveIndex;

        public int GetBestMoveIndex(Position position, Element elementWhichMove, int depth)
        {
            MinMax(position, elementWhichMove, depth);
            return _bestMoveIndex;
        }

        private int MinMax(Position position, Element elementWhichMove, int depth)
        {
            if (position.IsTerminal() || depth == 0)
            {
                return position.GetEvaluation();
            }

            int bestEvaluation = 0;

            if (elementWhichMove == Element.X)
            {
                bestEvaluation = -100;
                for (int i = 0; i < position.Count(); i++)
                {
                    if (position[i] == Cell.Empty)
                    {
                        var childPosition = position.Copy();
                        childPosition[i] = Cell.X;

                        var evaluation = MinMax(childPosition, Element.O, depth - 1);

                        Console.WriteLine("{0} {1} {2}", elementWhichMove, depth, evaluation);

                        if (evaluation >= bestEvaluation)
                        {
                            bestEvaluation = evaluation;
                            _bestMoveIndex = i;
                            if (evaluation == 100)
                            {
                                return evaluation;
                            }
                        }
                    }
                }
            }
            if (elementWhichMove == Element.O)
            {
                bestEvaluation = 100;
                for (int i = 0; i < position.Count(); i++)
                {
                    if (position[i] == Cell.Empty)
                    {
                        var childPosition = position.Copy();
                        childPosition[i] = Cell.O;

                        var evaluation = MinMax(childPosition, Element.X, depth - 1);

                        Console.WriteLine("{0} {1} {2}", elementWhichMove, depth, evaluation);

                        if (evaluation <= bestEvaluation)
                        {
                            bestEvaluation = evaluation;
                            _bestMoveIndex = i;
                            if (evaluation == -100)
                            {
                                return evaluation;
                            }
                        }
                    }
                }
            }

            return bestEvaluation;
        }

        private List<Move> GetAvailableMoves(Position position, Cell element)
        {
            var moves = new List<Move>();
            
            return moves;
        }

        private Cell InvertElement(Cell element)
        {
            return element == Cell.X ? Cell.O : Cell.X;
        }
    }
}
