﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tick_tack_toe.GameLogic.Enums;

namespace tick_tack_toe.GameLogic
{
    public class Line
    {
        public Line(Cell first, Cell second, Cell third)
        {
            Cells = new Cell[] { first, second, third };
        }

        public Cell[] Cells { get; private set; } 
    }
}
