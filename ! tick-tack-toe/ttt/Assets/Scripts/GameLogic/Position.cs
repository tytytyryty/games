﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tick_tack_toe.GameLogic.Enums;

namespace tick_tack_toe.GameLogic
{
    public class Position : IEnumerable<Cell>
    {
        private const int EVAL_WIN_X = 100;
        private const int EVAL_2X_ON_LINE = 15;
        private const int EVAL_1X_ON_LINE = 5;
        private const int EVAL_DRAW = 0;
        private const int EVAL_1O_ON_LINE = -5;
        private const int EVAL_2O_ON_LINE = -15;
        private const int EVAL_WIN_O = -100;

        private Cell[] _cells = new Cell[9];

        public Position()
        {
        
        }

        public Position(Cell[] cells)
        {
            _cells = cells;
        }

        public bool IsTerminal()
        {
            return GetGameState() != GameState.None;   
        }

        public int GetEvaluation()
        {
            var gameState = GetGameState();
            
            if (gameState == GameState.Draw)
            {
                return EVAL_DRAW;
            }
            if (gameState == GameState.WinX)
            {
                return EVAL_WIN_X;
            }
            if (gameState == GameState.WinO)
            {
                return EVAL_WIN_O;
            }
            
            var evaluation = 0;
            foreach (var line in GetLines())
            {
                evaluation += GetLineEvaluation(line);
            }
            return evaluation;
        }

        public Position Copy()
        {
            return new Position(this._cells.ToArray());
        }

        //public List<Position> GetChildPositions(Cell elementWhichMove)
        //{
        //    var childPositions = new List<Position>();
        //    for (int i = 0; i < this.Count(); i++)
        //    {
        //        if (this[i] == Cell.Empty)
        //        {
        //            var childPosition = new Position(this._cells);
        //            childPosition[i] = elementWhichMove;
        //            childPositions.Add(childPosition);
        //        }
        //    }
        //    return childPositions;
        //}

        public GameState GetGameState()
        {
            foreach (var line in GetLines())
            {
                if (CellsEqualAndNotEmpty(line.Cells))
                {
                    switch (line.Cells.First())
                    {
                        case Cell.X: return GameState.WinX;
                        case Cell.O: return GameState.WinO;
                    }
                }                
            }
            if (this.Count(i => i == Cell.Empty) == 0)
            {
                return GameState.Draw;
            }
            return GameState.None;
        }

        private List<Line> GetLines()
        {
            return new List<Line> 
            {
		        //rows
		        new Line (this[0], this[1], this[2]),
		        new Line (this[3], this[4], this[5]),
		        new Line (this[6], this[7], this[8]),
		        //columns
		        new Line (this[0], this[3], this[6]),
		        new Line (this[1], this[4], this[7]),
		        new Line (this[2], this[5], this[8]),
		        //diagonals
		        new Line (this[0], this[4], this[8]),
		        new Line (this[2], this[4], this[6])
	        };
        }

        private int GetLineEvaluation(Line line)
        {
            var xCountOnLine = line.Cells.Count(i => i == Cell.X);
            var oCountOnLine = line.Cells.Count(i => i == Cell.O);
            var emptyCountOnLine = line.Cells.Count(i => i == Cell.Empty);
            
            if (emptyCountOnLine == 3)
            {
                return EVAL_DRAW;
            }

            if (emptyCountOnLine == 2)
            {
                if (xCountOnLine == 1)
                {
                    return EVAL_1X_ON_LINE;
                }
                if (oCountOnLine == 1)
                {
                    return EVAL_1O_ON_LINE;
                }
            }

            if (emptyCountOnLine == 1)
            {
                if (xCountOnLine == 2)
                {
                    return EVAL_2X_ON_LINE;
                }
                if (oCountOnLine == 2)
                {
                    return EVAL_2O_ON_LINE;
                }
            }

            return EVAL_DRAW;
        }

        private bool CellsEqualAndNotEmpty(Cell[] cells)
        {
            return cells.Distinct().Count() == 1 && cells.First() != Cell.Empty;
        }

        public Cell this[int index]
        {
            get
            {
                return _cells[index];
            }
            set
            {
                _cells[index] = value;
            }
        }

        public IEnumerator<Cell> GetEnumerator()
        {
            return _cells.AsEnumerable<Cell>().GetEnumerator();
        }

        IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
