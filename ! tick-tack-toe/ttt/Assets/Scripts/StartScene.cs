﻿using tick_tack_toe.GameLogic.Enums;
using UnityEngine;
using UnityEngine.UI;

public class StartScene : MonoBehaviour {

    public int playWithIndex = 0;
    public string[] playWithOptions = { "X", "O" };
    //public Texture[] playWithTextures = new Texture[] { "X", "O" };

	// Use this for initialization
	void OnEnable () {
        var btnStartTransform = gameObject.transform.FindChild("BtnStart");
        var btnStart = btnStartTransform.GetComponent<Button>();
        btnStart.onClick.AddListener(StartGame);

        var btnQuitTransform = gameObject.transform.FindChild("BtnQuit");
        var btnQuit = btnQuitTransform.GetComponent<Button>();
        btnQuit.onClick.AddListener(Application.Quit);
	}
    
    void OnGUI()
    {
        //playWithIndex = GUI.SelectionGrid(new Rect(65, 50, 100, 30), playWithIndex, playWithOptions, 2);
    }

    private void StartGame()
    {
        GameSettings.PlayerPlayWith = (Element) playWithIndex;

        Application.LoadLevel("main");
    }
}
