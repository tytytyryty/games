﻿using UnityEngine;

public class CellController : MonoBehaviour
{
    public int Number;
    public CellsController cellsController;

    public void OnMouseDown() {
        if (!cellsController.DisabledCells)
        {
            if (!cellsController.IsEndOfGame() && (!cellsController.IsFilledCell(Number - 1)))
            {
                cellsController.DisabledCells = true;
                
                cellsController.FillCell(Number - 1);

                //cells.SetActive(false);
                StartCoroutine(cellsController.DoAIMove());
                //cells.SetActive(true);
                //cellsController.DoAIMove();
            }
        }
    }
}
