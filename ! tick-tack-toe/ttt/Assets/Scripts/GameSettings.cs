﻿using tick_tack_toe.GameLogic.Enums;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public static Element PlayerPlayWith;
    
    public void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
