﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace tick_tack_toe.GameLogic.Enums
{
    public enum Cell
    {
        Empty,
        X,
        O
    }
}
