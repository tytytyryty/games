﻿using UnityEngine;
using System.Collections;
using tick_tack_toe.GameLogic;
using tick_tack_toe.GameLogic.Enums;

public class CellsController : MonoBehaviour {

	public Texture xTexture;
	public Texture oTexture;
	public Texture lineXTexture;
	public Texture lineOTexture;
	public Material filledCellMaterial;
	public GameObject linePrefab;
    public bool DisabledCells = false;

	//Texture currentTexture;
    Element elementWhichMove;
	
    Position cells = new Position();
	
    int filledCells = 0;
	bool isEndOfGame = false;

	// Use this for initialization
	void Start () {

        elementWhichMove = GameSettings.PlayerPlayWith;

        //PlayerPrefs.SetInt("PlaysWith", (int)Cell.X);

	    //TODO: for X start
	    //DoAIMove();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

	public void FillCell(int cellIndex)
	{
        var cell = gameObject.transform.FindChild("cell" + cellIndex);
	    var anim = cell.GetComponent<Animator>();
        
        //cell.renderer.material = filledCellMaterial;
        //cell.renderer.material.mainTexture = currentTexture;
        if (elementWhichMove == Element.X)
        {
            cells[cellIndex] = Cell.X;
            //currentTexture = oTexture;
            anim.SetTrigger("fillX");
            elementWhichMove = Element.O;
        } 
        else 
        {
            cells[cellIndex] = Cell.O;
            //currentTexture = xTexture;
            anim.SetTrigger("fillO");
            elementWhichMove = Element.X;
        }
		filledCells++;
        CheckEndOfGame();
	}

    public IEnumerator DoAIMove()
    {
        return DoAIMoveWait();
    }

    public bool IsEndOfGame()
    {
        return isEndOfGame;
    }

    public bool IsFilledCell(int cellIndex)
    {
        return cells[cellIndex] != Cell.Empty;
    }

    IEnumerator DoAIMoveWait()
    {
        yield return new WaitForSeconds(1);
        if (!IsEndOfGame())
        {
            var aiMoveIndex = new AI().GetBestMoveIndex(cells, elementWhichMove, 3);
            FillCell(aiMoveIndex);
            DisabledCells = false;
        }
    }

    IEnumerator ShowEndGameLineWait(int rotation, Vector3 position, Cell winner)
    {
        yield return new WaitForSeconds(1);
        ShowEndGameLine(rotation, position, winner);
    }

    private void CheckEndOfGame()
	{
		if (cells [0] != Cell.Empty && cells [0] == cells [1] && cells [1] == cells [2]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(90, new Vector3(0,0.1f,0), cells [0]));
		}
		if (cells [3] != Cell.Empty && cells [3] == cells [4] && cells [4] == cells [5]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(90, new Vector3(0,0,0), cells [3]));
		}
		if (cells [6] != Cell.Empty && cells [6] == cells [7] && cells [7] == cells [8]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(90, new Vector3(0,-0.1f,0), cells [6]));
		}
		if (cells [0] != Cell.Empty && cells [0] == cells [3] && cells [3] == cells [6]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(0, new Vector3(-0.1f,0,0), cells [0]));
		}
		if (cells [1] != Cell.Empty && cells [1] == cells [4] && cells [4] == cells [7]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(0, new Vector3(0,0,0), cells [1]));
		}
		if (cells [2] != Cell.Empty && cells [2] == cells [5] && cells [5] == cells [8]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(0, new Vector3(0.1f,0,0), cells [2]));
		}
		if (cells [0] != Cell.Empty && cells [0] == cells [4] && cells [4] == cells [8]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(-45, new Vector3(0,0,0), cells [0]));
		}
		if (cells [2] != Cell.Empty && cells [2] == cells [4] && cells [4] == cells [6]) 
		{
			isEndOfGame = true;
            StartCoroutine(ShowEndGameLineWait(45, new Vector3(0,0,0), cells [2]));
		}

		if (filledCells == 9) {
			isEndOfGame = true;
		}

        if (isEndOfGame)
        {
            StartCoroutine(LoadEndScene());   
        }
	}

    IEnumerator LoadEndScene()
    {
        yield return new WaitForSeconds(2);

        PlayerPrefs.SetInt("GameResult", (int)cells.GetGameState());

        Application.LoadLevel("end");
    }

	private void ShowEndGameLine(int rotation, Vector3 position, Cell winner)
	{
		var lineContainer = Instantiate(linePrefab) as GameObject;
		lineContainer.transform.parent = gameObject.transform;
		lineContainer.transform.localPosition = position;
		lineContainer.transform.localScale = new Vector3(1,1,1);
		lineContainer.transform.Rotate(90,rotation,0);

		var line = lineContainer.transform.FindChild ("line");
		line.GetComponent<Renderer>().material = filledCellMaterial;
		if (winner == Cell.X) {
			line.GetComponent<Renderer>().material.mainTexture = lineXTexture;
		}
		if (winner == Cell.O) {
			line.GetComponent<Renderer>().material.mainTexture = lineOTexture;
		}
	}
}
