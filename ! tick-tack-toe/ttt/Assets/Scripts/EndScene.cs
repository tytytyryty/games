﻿using System.Collections;
using tick_tack_toe.GameLogic.Enums;
using UnityEngine;
using UnityEngine.UI;

public class EndScene : MonoBehaviour
{
    public Text GameResultText;

	// Use this for initialization
	void Start ()
	{
        var gameResult = (GameState)PlayerPrefs.GetInt("GameResult");
	    if (gameResult == GameState.Draw)
	    {
	        GameResultText.color = Color.yellow;
	        GameResultText.text = "Draw";
	    }
	    else
	    {
            //var playerPlayedWith = (Cell)PlayerPrefs.GetInt("PlaysWith");

            var playerPlayedWith = GameSettings.PlayerPlayWith;
	        if (playerPlayedWith == Element.X && gameResult == GameState.WinX ||
                playerPlayedWith == Element.O && gameResult == GameState.WinO)
	        {
                GameResultText.color = Color.green;
                GameResultText.text = "You win";
	        }
	        else
	        {
                GameResultText.color = Color.red;
                GameResultText.text = "You lose";
	        }
	    }

        StartCoroutine(LoadStartScene());   
	}

    IEnumerator LoadStartScene()
    {
        yield return new WaitForSeconds(2);

        Application.LoadLevel("start");
    }
}
