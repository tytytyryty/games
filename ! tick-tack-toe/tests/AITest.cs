﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using tick_tack_toe.GameLogic;
using tick_tack_toe.GameLogic.Enums;

namespace tests
{
    [TestClass]
    public class AITest
    {
        private Position eXeeeXOOe = new Position(new[]
		    {
			    Cell.Empty, Cell.X,     Cell.Empty,
			    Cell.Empty, Cell.Empty, Cell.X, 
			    Cell.O,     Cell.O,     Cell.Empty
		    }
        );

        private Position XOXeeeeee = new Position(new[]
		    {
			    Cell.X,     Cell.O,     Cell.X,
			    Cell.Empty, Cell.Empty, Cell.Empty, 
			    Cell.Empty, Cell.Empty, Cell.O
		    }
        );

        private Position eOOXeeeXe = new Position(new[]
		    {
			    Cell.Empty, Cell.O,     Cell.O,
			    Cell.X,     Cell.Empty, Cell.Empty, 
			    Cell.Empty, Cell.X,     Cell.Empty
		    }
        );

        private Position XXOeXeOee = new Position(new[]
		    {
			    Cell.X,     Cell.X,     Cell.O,
			    Cell.Empty, Cell.X,     Cell.Empty, 
			    Cell.O,     Cell.Empty, Cell.Empty
		    }
        );

        private Position XeOOXXeXO = new Position(new[]
		    {
			    Cell.X,     Cell.Empty, Cell.O,
			    Cell.O,     Cell.X,     Cell.X, 
			    Cell.Empty, Cell.X,     Cell.O
		    }
        );

        [TestMethod]
        public void GetBestMoveIndex_should_return_8_for_position_eXeeeXOOe_move_X_depth_2()
        {
            //setup
            var position = eXeeeXOOe;
            var move = Element.X;
            var depth = 2;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 8);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_8_for_position_eXeeeXOOe_move_X_depth_3()
        {
            //setup
            var position = eXeeeXOOe;
            var move = Element.X;
            var depth = 3;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 8);
        }


        [TestMethod]
        public void GetBestMoveIndex_should_return_8_for_position_eXeeeXOOe_move_O_depth_2()
        {
            //setup
            var position = eXeeeXOOe;
            var move = Element.O;
            var depth = 2;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 8);
        }

        //[TestMethod]
        public void GetBestMoveIndex_should_return_8_for_position_eXeeeXOOe_move_O_depth_1()
        {
            //setup
            var position = eXeeeXOOe;
            var move = Element.O;
            var depth = 3;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 8);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_6_for_position_XOXeeeeee_move_O_depth_2()
        {
            //setup
            var position = XOXeeeeee;
            var move = Element.O;
            var depth = 2;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 6);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_7_for_position_XOXeeeeee_move_O_depth_3()
        {
            //setup
            var position = XOXeeeeee;
            var move = Element.O;
            var depth = 3;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 7);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_0_for_position_eOOXeeeXe_move_O_depth_2()
        {
            //setup
            var position = eOOXeeeXe;
            var move = Element.O;
            var depth = 2;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 0);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_0_for_position_eOOXeeeXe_move_O_depth_3()
        {
            //setup
            var position = eOOXeeeXe;
            var move = Element.O;
            var depth = 3;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 0);
        }

        //[TestMethod]
        public void GetBestMoveIndex_should_return_7_for_position_XXOeXeOee_move_O_depth_2()
        {
            //setup
            var position = XXOeXeOee;
            var move = Element.O;
            var depth = 2;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 7);
        }

        [TestMethod]
        public void GetBestMoveIndex_should_return_1_for_position_XeOOXXeXO_move_O_depth_3()
        {
            //setup
            var position = XeOOXXeXO;
            var move = Element.O;
            var depth = 3;

            //action
            var bestMoveIndex = new AI().GetBestMoveIndex(position, move, depth);

            //assertion
            Assert.AreEqual(bestMoveIndex, 1);
        }
    }
}
