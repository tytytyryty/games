﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{

    public float cameraSmoothTime = 0.2f;
    public float minCameraSize = 6f;
    public float minDistanceFromEdge = 2f;
    public Transform[] players;

    float zoomSpeed;
    Camera camera;
    Transform cameraRig;
    //Vector3 cameraOffset;

    void Start()
    {
        camera = GetComponentInChildren<Camera>();
        cameraRig = transform;
        //cameraOffset = camera.transform.position;
    }

    void FixedUpdate()
    {
        Move();
        Zoom();
    }

    private void Move()
    {
        cameraRig.position = GetAveragePosition();
    }

    private Vector3 GetAveragePosition()
    {
        var averagePosition = Vector3.zero;
        var positionSum = Vector3.zero;

        foreach (var player in players)
        {
            positionSum += player.position;
        }

        if (players.Length > 0)
        {
            averagePosition = positionSum / players.Length;
        }

        //averagePosition += cameraOffset;
        averagePosition.y = cameraRig.position.y;

        return averagePosition;
    }

    private void Zoom()
    {
        // width/height 1.7778 for 16:9
        //camera.aspect

        //Debug.Log(camera.WorldToViewportPoint(players[0].position));
        //Debug.Log(camera.WorldToScreenPoint(players[0].position));

        //var viewHeight = 2f * camera.orthographicSize;
        //var viewWidth = viewHeight * camera.aspect;

        var size = 0f;

        //Debug.Log(string.Format("Camera rig global: {0}, local: {1}", cameraRig.position, transform.InverseTransformPoint(cameraRig.position)));
        //Debug.Log(string.Format("Player 1 global: {0}, local: {1}", players[0].position, transform.InverseTransformPoint(players[0].position)));
        //Debug.Log(string.Format("Player 2 global: {0}, local: {1}", players[1].position, transform.InverseTransformPoint(players[1].position)));

        foreach (var player in players)
        {
            //Vector3 headingDirection = cameraRig.position - player.position;

            //size = Mathf.Max(size, Mathf.Abs(headingDirection.z));
            //size = Mathf.Max(size, Mathf.Abs(headingDirection.x) / camera.aspect);

            Vector3 headingDirection =
                cameraRig.InverseTransformPoint(cameraRig.position) - cameraRig.InverseTransformPoint(player.position);

            size = Mathf.Max(size, Mathf.Abs(headingDirection.y));
            size = Mathf.Max(size, Mathf.Abs(headingDirection.x) / camera.aspect);
        }

        size += minDistanceFromEdge;
        size = Mathf.Max(size, minCameraSize);

        camera.orthographicSize = Mathf.SmoothDamp(camera.orthographicSize, size, ref zoomSpeed, cameraSmoothTime);

        //Debug.Log("viewHeight " + viewHeight);
        //Debug.Log("edgePosZ " + edgePosZ);
        //Debug.Log("players[0].position.z " + players[0].position.z);
        //Debug.Log("sizeToIncreaseFromHeight " + sizeToIncreaseFromHeight);
    }

    public void SetStartPositionAndSize()
    {
        //Move();
        //Zoom();
    }
}
