﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool
{
    private Dictionary<string, Queue<GameObject>> _pool 
             = new Dictionary<string, Queue<GameObject>>();

    private int _instantiatedObjects;
    
    public GameObject CreateOrGetFromPool(GameObject prefabObj)
    {
        Queue<GameObject> listOfObjects;
        _pool.TryGetValue(prefabObj.name + "(Clone)", out listOfObjects);

        if (listOfObjects.IsNullOrEmpty())
        {
            _instantiatedObjects++;
            return Object.Instantiate(prefabObj);
        }

        var instantiatedObj = listOfObjects.Dequeue();
        instantiatedObj.SetActive(true);
        return instantiatedObj;
    }

    public void ReturnToPool(GameObject instantiatedObj)
    {
        Queue<GameObject> listOfObjects;
        if (!_pool.TryGetValue(instantiatedObj.name, out listOfObjects))
        {
            listOfObjects = new Queue<GameObject>();
            _pool[instantiatedObj.name] = listOfObjects;
        }
        
        instantiatedObj.SetActive(false);
        listOfObjects.Enqueue(instantiatedObj);
    }

    public override string ToString()
    {
        var str = "";
        foreach (var item in _pool)
        {
            str += item.Key + " " + item.Value.Count + "\r\n";
        }
        return str;
    }
}
