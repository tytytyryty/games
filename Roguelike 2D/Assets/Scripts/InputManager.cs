﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    private GameManager gameManager;
    private BoardConstructor boardConstructor;

    void Update()
    {
        if (Input.GetKey(KeyCode.I))
        {
            if (gameManager == null)
            {
                gameManager = FindObjectOfType<GameManager>();
                boardConstructor = gameManager.GetComponent<BoardConstructor>();
            }
            boardConstructor.CreateBoard(GameManager.Instance.Level);
        }

        if (Input.GetKey(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

}
