﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GameManager : MonoBehaviour {

    public int Level { get; private set; }
    public bool PlayerTurnToMove { get; set; }
    public bool LevelIsLoading { get; private set; }    
    
    private BoardConstructor boardConstructor;
    private UIManager uiManager;
    private List<Enemy> enemies = new List<Enemy>();

	void Start ()
    {
        Instance = this;

        boardConstructor = GetComponent<BoardConstructor>();
        uiManager = GetComponent<UIManager>();

        PlayerTurnToMove = true;
        LoadNextLevel();
	}

    void Update ()
    {
        if (!PlayerTurnToMove)
        {
            MoveEnemies();
        }
    }

    public static GameManager Instance { get; private set; }

    public void LoadNextLevel()
    {
        Level++;

        StartCoroutine(LoadNextLevelCoroutine());
    }
    
    public void AddEnemyToList(Enemy enemy)
    {
        enemies.Add(enemy);
    }

    private IEnumerator LoadNextLevelCoroutine()
    {
        LevelIsLoading = true;
        yield return uiManager.InitLevelStartScreenCoroutine(Level);

        enemies.Clear();
        boardConstructor.CreateBoard(Level);
        uiManager.SubscribeToEvents();
        this.SubscribeForEvents();

        yield return uiManager.FadeLevelStartScreenCoroutine();
        LevelIsLoading = false;
        SoundManager.Instance.BackgroundMusic();
    }

    private void GameOver()
    {
        LevelIsLoading = true;
        SoundManager.Instance.GameOver();
        StartCoroutine(uiManager.InitGameOverScreenCoroutine(Level));
    }

    private void SubscribeForEvents()
    {
        var player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.onFoodChanged += (food) => 
        {
            if (food <= 0)
            {
                GameOver();
            }
        };
    }

    private void MoveEnemies()
    {
        foreach (var enemy in enemies)
        {
            enemy.DoMove();
        }

        PlayerTurnToMove = true;
    }
}
