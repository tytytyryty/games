﻿using UnityEngine;

public class SoundManager : MonoBehaviour 
{
    public static SoundManager Instance = null;     //Allows other scripts to call functions from SoundManager.				

    public AudioSource efxSource;					//Drag a reference to the audio source which will play the sound effects.
	public AudioSource musicSource;					//Drag a reference to the audio source which will play the music.
	public float lowPitchRange = .95f;				//The lowest a sound effect will be randomly pitched.
	public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.

    public AudioClip chopSound1;                //1 of 2 audio clips that play when the wall is attacked by the player.
    public AudioClip chopSound2;                //2 of 2 audio clips that play when the wall is attacked by the player.
    public AudioClip moveSound1;                //1 of 2 Audio clips to play when player moves.
    public AudioClip moveSound2;                //2 of 2 Audio clips to play when player moves.
    public AudioClip eatSound1;                 //1 of 2 Audio clips to play when player collects a food object.
    public AudioClip eatSound2;                 //2 of 2 Audio clips to play when player collects a food object.
    public AudioClip drinkSound1;               //1 of 2 Audio clips to play when player collects a soda object.
    public AudioClip drinkSound2;               //2 of 2 Audio clips to play when player collects a soda object.
    public AudioClip gameOverSound;				//Audio clip to play when player dies.
    public AudioClip attackSound1;                      //First of two audio clips to play when attacking the player.
    public AudioClip attackSound2;                      //Second of two audio clips to play when attacking the player.

    void Awake ()
	{
		if (Instance == null)
			Instance = this;
		//If instance already exists:
	}
	
    public void BackgroundMusic()
    {
        musicSource.Play();
    }

    public void PlayerMove()
    {
        RandomizeSfx(moveSound1, moveSound2);
    }

    public void PlayerChop()
    {
        RandomizeSfx(chopSound1, chopSound2);
    }

    public void PlayerEat()
    {
        RandomizeSfx(eatSound1, eatSound2);
    }

    public void PlayerDrink()
    {
        RandomizeSfx(drinkSound1, drinkSound2);
    }
    
    public void EnemyAttack()
    {
        RandomizeSfx(attackSound1, attackSound2);
    }

    public void GameOver()
    {
        PlaySingle(gameOverSound);
    }

	private void PlaySingle(AudioClip clip)
	{
		//Set the clip of our efxSource audio source to the clip passed in as a parameter.
		efxSource.clip = clip;
			
		//Play the clip.
		efxSource.Play ();
	}
		
		
	//RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
	private void RandomizeSfx (params AudioClip[] clips)
	{
		//Generate a random number between 0 and the length of our array of clips passed in.
		int randomIndex = Random.Range(0, clips.Length);
			
		//Choose a random pitch to play back our clip at between our high and low pitch ranges.
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);
			
		//Set the pitch of the audio source to the randomly chosen pitch.
		efxSource.pitch = randomPitch;
			
		//Set the clip to the clip at our randomly chosen index.
		efxSource.clip = clips[randomIndex];
			
		//Play the clip.
		efxSource.Play();
	}
}
