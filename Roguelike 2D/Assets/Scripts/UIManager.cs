﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {
    
    public Text foodText;
    public Text levelText;
    public Text gameOverText;
    public Image levelInitImage;
    public float levelStartScreenAfter;
    public float levelStartScreenDuration;
    public float levelStartTextFadeAfter;

    public void SubscribeToEvents()
    {
        var player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        player.onFoodChanged += (food) => foodText.text = "Food " + Mathf.Clamp(food, 0, food);
    }

    public IEnumerator InitLevelStartScreenCoroutine(int level)
    {
        yield return new WaitForSeconds(levelStartScreenAfter);

        levelInitImage.color = Color.black;
        levelText.text = "Day " + level;
    }

    public IEnumerator FadeLevelStartScreenCoroutine()
    {
        yield return new WaitForSeconds(levelStartTextFadeAfter);

        float fadeDuration = levelStartScreenDuration - levelStartTextFadeAfter;
        float elapsedTime = 0;
        while (elapsedTime < fadeDuration)
        {
            elapsedTime += Time.deltaTime;
            levelText.color = Color.Lerp(Color.white, Color.black, elapsedTime / fadeDuration);
            yield return null;
        }

        levelInitImage.color = Color.clear;
        levelText.text = "";
    }

    public IEnumerator InitGameOverScreenCoroutine(int level)
    {
        yield return new WaitForSeconds(levelStartScreenAfter);

        levelInitImage.color = Color.black;
        gameOverText.text = "After " + level + " days you starved...";
    }
}
