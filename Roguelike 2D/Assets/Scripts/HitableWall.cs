﻿using UnityEngine;

public class HitableWall : MonoBehaviour
{
    public int startHealth;
    public int healthPerDamage;
    public Sprite damageSprite;

    private int health;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        health = startHealth;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void TakeDamage()
    {
        health -= healthPerDamage;
        spriteRenderer.sprite = damageSprite;
        if (health <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}

