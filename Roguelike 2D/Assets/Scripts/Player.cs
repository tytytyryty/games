﻿using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    public LayerMask blockingLayer;
    public float timeBetweenMoves;
    public int startFood;
    public int foodPointsForMove;
    public int foodPointsForFood;
    public int foodPointsForSoda;
    public int foodPointsWhenAttacked;
    public event Action<int> onFoodChanged;
    public int Food
    {
        get { return _food; }
        private set
        {
            _food = value;
            onFoodChanged.Raise(_food);
        }
    }

    private int _food;
    private float _lastMoveTime = 0;
    private BoxCollider2D _boxCollider2D;
    private Animator _animator;

    void Start()
    {
        Food = startFood;
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Time.time > _lastMoveTime + timeBetweenMoves && !GameManager.Instance.LevelIsLoading)
        {
            var horizontalMove = (int)Input.GetAxisRaw("Horizontal");
            var verticalMove = (int)Input.GetAxisRaw("Vertical");

            if (horizontalMove != 0)
            {
                verticalMove = 0;
            }

            if (horizontalMove != 0 || verticalMove != 0)
            {
                AttemptMove(horizontalMove, verticalMove);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit")
        {
            GameManager.Instance.LoadNextLevel();
        }
        if (other.tag == "Food")
        {
            Food += foodPointsForFood;
            other.gameObject.SetActive(false);
            SoundManager.Instance.PlayerEat();
        }
        if (other.tag == "Soda")
        {
            Food += foodPointsForSoda;
            other.gameObject.SetActive(false);
            SoundManager.Instance.PlayerDrink();
        }
    }

    public void TakeDamage()
    {
        Food -= foodPointsWhenAttacked;
        _animator.SetTrigger("Hit");
    }

    private void AttemptMove(int horizontal, int vertical)
    {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(horizontal, vertical);
        _boxCollider2D.enabled = false;
        var obstacle = Physics2D.Linecast(start, end, blockingLayer);
        _boxCollider2D.enabled = true;
        if (!obstacle)
        {
            DoMove(horizontal, vertical);
        }
        else
        {
            var hitableWall = obstacle.transform.GetComponent<HitableWall>();
            if (hitableWall)
            {
                _animator.SetTrigger("Chop");
                SoundManager.Instance.PlayerChop();
                hitableWall.TakeDamage();
                CompleteMoveAction();
            }
        }
    }
    
    private void DoMove(int horizontal, int vertical)
    {
        transform.position = new Vector3(
            transform.position.x + horizontal,
            transform.position.y + vertical, 0);
        SoundManager.Instance.PlayerMove();
        CompleteMoveAction();
    }

    private void CompleteMoveAction()
    {
        Food -= foodPointsForMove;
        _lastMoveTime = Time.time;
        GameManager.Instance.PlayerTurnToMove = false;
    }
}
