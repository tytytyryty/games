﻿using System;

static public class EventExtensions
{
    static public void Raise<T>(this Action<T> eve, T par)
    {
        if (eve != null)
            eve(par);
    }
}
