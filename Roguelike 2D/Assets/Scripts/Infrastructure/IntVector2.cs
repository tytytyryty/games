﻿using System;

[Serializable]
public class IntVector2
{
    public int x;
    public int y;
    
    public IntVector2(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override bool Equals(object obj)
    {
        return Equals((IntVector2) obj);
    }

    public override int GetHashCode()
    {
        unchecked 
        {
            int hash = 17;
            hash = hash * 23 + x.GetHashCode();
            hash = hash * 23 + y.GetHashCode();
            return hash;
        }
    }

    public bool Equals(IntVector2 other)
    {
        if (this.x == other.x && this.y == other.y)
        {
            return true;
        }

        return base.Equals(other);
    }

    public static bool operator == (IntVector2 vector1, IntVector2 vector2)
    {
        return vector1.Equals(vector2);
    }

    public static bool operator != (IntVector2 vector1, IntVector2 vector2)
    {
        return !vector1.Equals(vector2);
    }
}
