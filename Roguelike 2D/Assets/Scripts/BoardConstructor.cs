﻿using UnityEngine;
using System.Collections.Generic;

public class BoardConstructor : MonoBehaviour {

    public int level;
    public int areaLength;
    public IntVector2 playerStartPosition;
    public IntVector2 exitPosition;
    public GameObject[] OuterWalls;
    public GameObject[] InnerWalls;
    public GameObject[] Floors;
    public GameObject[] Foods;
    public GameObject Exit;
    public GameObject Player;
    public GameObject[] Enemies;
    
    private int leftEdge;
    private int rightEdge;

    private List<IntVector2> occupiedPositions = new List<IntVector2>();
    private ObjectPool objectPool = new ObjectPool();
    
    public void CreateBoard(int level)
    {
        destroyExistingSprites();
        occupiedPositions.Clear();

        leftEdge = -areaLength / 2 + 1;
        rightEdge = areaLength / 2 - 1;

        instantiateOuterWallsAndFloor();
        instantiateExit(exitPosition.x, exitPosition.y);
        instantiatePlayer(playerStartPosition.x, playerStartPosition.y);
        instantiateEnemies(level);
        instantiateFoods();
        instantiateInnerWalls();
    }
    
    private void instantiateOuterWallsAndFloor()
    {
        for (int x = leftEdge; x <= rightEdge; x++)
        {
            for (int y = leftEdge; y <= rightEdge; y++)
            {
                if (isOuterWall(x, y))
                {
                    instantiateOuterWall(x, y);
                }
                else
                {
                    instantiateFloor(x, y);
                }
            }
        }
    }

    private bool isOuterWall(int x, int y)
    {
        return x == leftEdge || x == rightEdge || y == leftEdge || y == rightEdge;
    }

    private void instantiateOuterWall(int x, int y)
    {
        instantiateTile(OuterWalls[Random.Range(0, OuterWalls.Length)], x, y);
    }

    private void instantiateFloors()
    {
        for (int x = leftEdge + 1; x <= rightEdge - 1; x++)
        {
            for (int y = leftEdge + 1; y <= rightEdge - 1; y++)
            {
                instantiateFloor(x, y);
            }
        }
    }

    private void instantiateFloor(int x, int y)
    {
        instantiateTile(Floors[Random.Range(0, Floors.Length)], x, y);
    }

    private void instantiateInnerWalls()
    {
        int wallsCount = Random.Range(3, 5);
        for (int i = 0; i < wallsCount; i++)
        {
            var x = Random.Range(leftEdge + 1, rightEdge - 1);
            var y = Random.Range(leftEdge + 1, rightEdge - 1);
            
            instantiateInnerWall(x, y);
        }
    }

    private void instantiateInnerWall(int x, int y)
    {
        instantiateTile(InnerWalls[Random.Range(0, InnerWalls.Length)], x, y, true);
    }

    private void instantiateFoods()
    {
        int foodsCount = Random.Range(0, 3);
        for (int i = 0; i < foodsCount; i++)
        {
            var x = Random.Range(leftEdge + 1, rightEdge - 1);
            var y = Random.Range(leftEdge + 1, rightEdge - 1);
            instantiateFood(x, y);
        }
    }

    private void instantiateFood(int x, int y)
    {
        instantiateTile(Foods[Random.Range(0, Foods.Length)], x, y, true);
    }

    private void instantiateExit(int x, int y)
    {
        instantiateTile(Exit, x, y, true);
    }

    private void instantiatePlayer(int x, int y)
    {
        instantiateTile(Player, x, y, true);
    }

    private void instantiateEnemies(int level)
    {
        int enemiesCount = (int) Mathf.Log(level, 2f);
        for (int i = 0; i < enemiesCount; i++)
        {
            var x = Random.Range(leftEdge + 1, rightEdge - 1);
            var y = Random.Range(leftEdge + 1, rightEdge - 1);
            instantiateEnemy(x, y);
        }
    }

    private void instantiateEnemy(int x, int y)
    {
        var instantiatedEnemy = instantiateTile(Enemies[Random.Range(0, Enemies.Length)], x, y, true);
        if (instantiatedEnemy != null)
        {
            GameManager.Instance.AddEnemyToList(instantiatedEnemy.GetComponent<Enemy>());
        }
    }

    private GameObject instantiateTile(GameObject tile, int x, int y, bool occupied = false)
    {
        GameObject instantiatedTile = null;
        if (!occupied)
        {
            instantiatedTile = objectPool.CreateOrGetFromPool(tile);
            instantiatedTile.transform.position = new Vector3(x, y, 0f);
        }
        else
        {
            if (!occupiedPositions.Contains(new IntVector2(x, y)))
            {
                instantiatedTile = objectPool.CreateOrGetFromPool(tile);
                instantiatedTile.transform.position = new Vector3(x, y, 0f);
                occupiedPositions.Add(new IntVector2(x, y));
            }
        }
        return instantiatedTile;
    }
    
    private void destroyExistingSprites()
    {
        foreach (var renderer in FindObjectsOfType<SpriteRenderer>())
        {
            objectPool.ReturnToPool(renderer.gameObject);
        }
    }
}
