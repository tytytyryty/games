﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public LayerMask blockingLayer;
    
    private Animator _animator;
    private BoxCollider2D _boxCollider2D;
    private Player _player;

    void Start()
    {
        _animator = GetComponent<Animator>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
        _player = FindObjectOfType<Player>();
    }
    
    public void DoMove()
    {
        Vector2 moveTowards = _player.transform.position - transform.position;
        moveTowards = moveTowards / Mathf.Max(Mathf.Abs(moveTowards.x), Mathf.Abs(moveTowards.y));

        var moveDiagonally = Mathf.Abs(moveTowards.x) == 1 && Mathf.Abs(moveTowards.y) == 1;
        if (moveDiagonally) {
            var randomVerticalHorizontal = Random.value;
            if (randomVerticalHorizontal > 0.5) {
                moveTowards.x = 0;
            }
            else {
                moveTowards.y = 0;
            }
        }
        if (Mathf.Abs(moveTowards.x) == 1) {
            moveTowards.y = 0;
        }
        else {
            moveTowards.x = 0;
        }

        Vector2 start = transform.position;
        Vector2 end = start + moveTowards;

        _boxCollider2D.enabled = false;
        var obstacle = Physics2D.Linecast(start, end, blockingLayer);
        _boxCollider2D.enabled = true;
        if (!obstacle)
        {
            var skipMove = Random.value > 0.5;
            if (!skipMove)
            {
                transform.position = new Vector3(
                    transform.position.x + moveTowards.x,
                    transform.position.y + moveTowards.y, 0);
            }
        }
        else
        {
            var isPlayer = obstacle.transform.GetComponent<Player>();
            if (isPlayer)
            {
                _animator.SetTrigger("Attack");
                SoundManager.Instance.EnemyAttack();
                _player.TakeDamage();
            }
        }
    }
}
