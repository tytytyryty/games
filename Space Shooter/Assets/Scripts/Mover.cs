﻿using UnityEngine;

public class Mover : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
        var rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, speed);
	}
}
