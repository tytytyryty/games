﻿using UnityEngine;

public class PlayerControllerMobileAcceleration : MonoBehaviour
{
    public int speed;
    public int tilt;
    public Boundary boundary;
    public GameObject bolt;
    public Transform shotSpawn;
    public float shotDelaySeconds;

    private Rigidbody rb;
    private float lastShotTime;
    private Vector3 startAcceleration;
    private Quaternion calibrationQuaternion;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startAcceleration = Input.acceleration;
        CalibrateAccelerometer();
        Debug.Log(startAcceleration);
    }

    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time - lastShotTime > shotDelaySeconds)
        {
            Instantiate(bolt, shotSpawn.position, shotSpawn.rotation);
            lastShotTime = Time.time;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var rawAcceleration = Input.acceleration;
        var acceleration = FixAcceleration(rawAcceleration);
        rb.velocity = new Vector3(acceleration.x, 0, acceleration.y) * speed;
        Debug.Log("Acc: " + acceleration + " Vel: " + rb.velocity);
        //rb.velocity = acceleration * speed;

        rb.rotation = Quaternion.Euler(0, 0, rb.velocity.x * -tilt);
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX),
            0,
            Mathf.Clamp(rb.position.z, boundary.minZ, boundary.maxZ)
        );
    }

    void CalibrateAccelerometer()
    {
        Vector3 accelerationSnapshot = Input.acceleration;
        Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0.0f, 0.0f, -1.0f), accelerationSnapshot);
        calibrationQuaternion = Quaternion.Inverse(rotateQuaternion);
    }

    //Get the 'calibrated' value from the Input
    Vector3 FixAcceleration(Vector3 acceleration)
    {
        Vector3 fixedAcceleration = calibrationQuaternion * acceleration;
        return fixedAcceleration;
    }
}
