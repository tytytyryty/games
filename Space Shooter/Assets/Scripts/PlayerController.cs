﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
    public int speed;
    public int tilt;
    public Boundary boundary;
    public GameObject bolt;
    public Transform shotSpawn;
    public float shotDelaySeconds;
    
    private Rigidbody rb;
    private float lastShotTime;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time - lastShotTime > shotDelaySeconds)
        {
            Instantiate(bolt, shotSpawn.position, shotSpawn.rotation);
            lastShotTime = Time.time;
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        rb.velocity = new Vector3(horizontal, 0, vertical) * speed;
        rb.rotation = Quaternion.Euler(0, 0, rb.velocity.x * -tilt);
        rb.position = new Vector3(
            Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX),
            rb.position.y,
            Mathf.Clamp(rb.position.z, boundary.minZ, boundary.maxZ)
        );
    }
}

[Serializable]
public class Boundary
{
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;
}