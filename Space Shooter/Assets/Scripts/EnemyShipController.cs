﻿using System.Collections;
using UnityEngine;

public class EnemyShipController : MonoBehaviour {
    
    public GameObject bolt;
    public Transform shotSpawn;
    public float shotFrequency;
    public float tilt;
    public float maneuverPos;
    public Boundary boundary;
    public Vector2 maneuverTime;
    public Vector2 maneuverWait;
    public float maneuverSmoothing;

    Rigidbody rb;
    private float currManeuverVelocity;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        InvokeRepeating("Fire", shotFrequency, shotFrequency);
        StartCoroutine(Maneuver());
    }
	
    IEnumerator Maneuver()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            currManeuverVelocity = Random.Range(-maneuverPos, maneuverPos);
            yield return new WaitForSeconds(Random.Range(maneuverTime.x, maneuverTime.y));
            currManeuverVelocity = 0;
            yield return new WaitForSeconds(Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        var newManeuverVelocity = Mathf.MoveTowards(rb.velocity.x, currManeuverVelocity, maneuverSmoothing);
        rb.velocity = new Vector3(newManeuverVelocity, rb.velocity.y, rb.velocity.z);
        ClampPosition(); //this is here because we set velocity of the ship so need to ensure it will not go off box
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, rb.velocity.x * tilt)); 
    }

    void Fire()
    {
        Instantiate(bolt, shotSpawn.position, shotSpawn.rotation);
    }

    private void ClampPosition()
    {
        rb.position = new Vector3
        (
            Mathf.Clamp(rb.position.x, boundary.minX, boundary.maxX),
            0.0f,
            Mathf.Clamp(rb.position.z, boundary.minZ, boundary.maxZ)
        );
    }
}
