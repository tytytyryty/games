﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public GameObject[] enemies;
    public EnemyStartPosition enemyStartPosition;
    public float waitForWave;
    public float waitForEnemyInWave;
    public int enemiesInOneWave;
    public Text ScoreText;
    public Text RestartText;
    public Text GameOverText;

    private bool gameOver;
    private bool restart;
    private int score;

    // Use this for initialization
    void Start() {
        gameOver = false;
        restart = false;
        score = 0;
        InitUIText();
        StartCoroutine(SpawnEnemies());
	}

    void Update()
    {
        if (restart && Input.GetKey(KeyCode.R))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void InitUIText()
    {
        UpdateScore(0);
        RestartText.text = "";
        GameOverText.text = "";
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            for (int i = 0; i < enemiesInOneWave; i++)
            {
                yield return new WaitForSeconds(waitForEnemyInWave);
                var enemy = enemies[Random.Range(0, enemies.Length)];
                Instantiate(enemy, new Vector3(Random.Range(enemyStartPosition.minX, enemyStartPosition.maxX), 0, enemyStartPosition.z), Quaternion.identity);
            }
            yield return new WaitForSeconds(waitForWave);

            if (gameOver)
            {
                RestartText.text = "Press 'R' to restart";
                restart = true;
                break;
            }
        }
    }

    public void GameOver()
    {
        GameOverText.text = "Game over";
        gameOver = true;
    }

    public void UpdateScore(int byValue)
    {
        score += byValue;
        ScoreText.text = "Score: " + score;
    }
}

[System.Serializable]
public class EnemyStartPosition
{
    public float z;
    public float minX;
    public float maxX;
}

