﻿using UnityEngine;

public class BackgroundMover : MonoBehaviour {

    public float speed;

    private float backgroundLength;
    private Vector3 startPosition;

	// Use this for initialization
	void Start () {
        backgroundLength = transform.localScale.y;
        startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {

        float newPositionZ = Mathf.Repeat(Time.time * speed, backgroundLength);
        transform.position = startPosition + Vector3.forward * newPositionZ;
    }
}
