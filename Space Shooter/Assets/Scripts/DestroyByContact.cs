﻿using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject explosionPlayer;
    public int increaseOfScore;

    private GameController gameController;

    void Start()
    {
        var gameControllerObj = GameObject.FindGameObjectWithTag("GameController");
        gameController = gameControllerObj.GetComponent<GameController>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameBox" || other.tag == "Enemy")
        {
            return;
        }

        Destroy(other.gameObject);
        Destroy(gameObject);
        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }

        if (other.tag == "Player")
        {
            Instantiate(explosionPlayer, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }
        else
        {
            gameController.UpdateScore(increaseOfScore);
        }
    }
}
