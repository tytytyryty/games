﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProjectileDragging : MonoBehaviour {

    public float maxStretch = 3f;
    public LineRenderer catapultLineFront;
    public LineRenderer catapultLineBack;

    private SpringJoint2D springJoint;
    private Transform catapult;
    private Rigidbody2D rigidBody;
    private CircleCollider2D circleCollider;
    private Transform projectileTransform;
    private Ray catapultToProjectileRay = new Ray();
    private Ray rayToMouse = new Ray();
    private float projectileRadius;
    private Vector3 projectileHoldPoint;
    private float projectileHoldDistance;
    private bool isDragging = false;
    private Vector2 previousVelocity;

    void Awake()
    {
        springJoint = GetComponent<SpringJoint2D>();
        rigidBody = GetComponent<Rigidbody2D>();
        circleCollider = GetComponent<CircleCollider2D>();
        projectileTransform = transform;
        projectileRadius = circleCollider.radius;

        catapult = springJoint.connectedBody.transform;
    }

    void Start()
    {
        LineRendererSetup();    
    }
    
    void Update()
    {
        if (isDragging)
        {
            Dragging();
        }
        
        if (!isDragging && springJoint.enabled == true)
        {
            if (!rigidBody.isKinematic && previousVelocity.magnitude > rigidBody.velocity.magnitude)
            {
                rigidBody.velocity = previousVelocity;
                springJoint.enabled = false;
                catapultLineFront.enabled = false;
                catapultLineBack.enabled = false;
                StartCoroutine(ReloadScene());
            }

            if (!isDragging)
            {
                previousVelocity = rigidBody.velocity;
            }
        }
        
        LineRendererUpdate();
    }

    void LineRendererSetup()
    {
        catapultLineFront.SetPosition(0, catapultLineFront.transform.position);
        catapultLineBack.SetPosition(0, catapultLineBack.transform.position);

        catapultLineFront.sortingLayerName = "catapult";
        catapultLineBack.sortingLayerName = "catapult";

        catapultLineFront.sortingOrder = 3;
        catapultLineBack.sortingOrder = 1;
    }

    void LineRendererUpdate()
    {
        Vector2 catapultToProjectile = transform.position - catapultLineFront.transform.position;
        var projectileHoldDistance = catapultToProjectile.magnitude + projectileRadius;

        catapultToProjectileRay.origin = catapultLineFront.transform.position;
        catapultToProjectileRay.direction = catapultToProjectile;
        var projectileHoldPoint = catapultToProjectileRay.GetPoint(projectileHoldDistance);

        catapultLineFront.SetPosition(1, projectileHoldPoint);
        catapultLineBack.SetPosition(1, projectileHoldPoint);
    }
    
    void Dragging()
    {
        //we can not pass just Input.mousePosition because it's z is always 0 and ScreenToWorldPoint will return z as camera position
        var mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPoint.z = 0;

        Vector2 catapultToMouse = mouseWorldPoint - catapultLineFront.transform.position;
        if (catapultToMouse.magnitude > maxStretch)
        {
            rayToMouse.origin = catapultLineFront.transform.position;
            rayToMouse.direction = catapultToMouse;
            mouseWorldPoint = rayToMouse.GetPoint(maxStretch);
        }

        transform.position = mouseWorldPoint;
    }

    void OnMouseUp()
    {
        isDragging = false;
        springJoint.enabled = true;
        rigidBody.isKinematic = false;
        previousVelocity = rigidBody.velocity;
    }

    void OnMouseDown()
    {
        isDragging = true;
        springJoint.enabled = false;
    }

    private IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(5);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
