﻿using UnityEngine;

public class Enemy : MonoBehaviour {

    public Sprite damageSprite;
    public int hitPoints;

    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "damager")
        {
            Debug.Log("Enter " + collision.gameObject.name);
            hitPoints--;
            Debug.Log("HitPoints " + hitPoints);
            spriteRenderer.sprite = damageSprite;

            if (hitPoints <= 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
