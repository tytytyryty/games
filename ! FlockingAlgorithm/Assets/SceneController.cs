﻿using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {

    public GameObject agent;
    public static List<Rigidbody2D> agents = new List<Rigidbody2D>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetButtonDown("Fire1"))
        {
            var objPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            objPosition.z = 0;
            //for (int i = 0; i < 5; i++)
            //{
            var fishClone = (GameObject)Instantiate(agent, objPosition, Quaternion.identity);
            var rigidbody = fishClone.GetComponent<Rigidbody2D>();
            agents.Add(rigidbody);
                
            //}
        }
	}
    
    void OnTriggerExit2D(Collider2D other)
    {
        var x = other.transform.position.x;
        var y = other.transform.position.y;
        other.transform.position = new Vector2(-x, -y);
    }
}
