﻿using UnityEngine;

public class FishController : MonoBehaviour {

    public float fishSpeed;
    public float radius;

    // Use this for initialization
    void Start () {
        transform.Rotate(0, 0, Random.Range(0, 360));
        GetComponent<Rigidbody2D>().AddForce(transform.right * fishSpeed, ForceMode2D.Impulse);
    }

	// Update is called once per frame
	void Update () {
        var agent = gameObject.GetComponent<Rigidbody2D>();
        steer(agent);
        //TODO unfolding from walls
        //if (rigidbody.drag != 0 && rigidbody.velocity.magnitude <= 2)
        //{
        //    rigidbody.transform.Rotate(0, 0, Random.Range(0, 360));
        //    rigidbody.AddForce(rigidbody.transform.right * rigidbody.velocity.magnitude, ForceMode2D.Impulse);
        //    rigidbody.drag = 0;
        //}
    }

    private void steer(Rigidbody2D agent)
    {
        var alignment = getAlignment(agent);
        var cohesion = getCohesion(agent);
        var separation = getSeparation(agent);

        var velocityX = agent.velocity.x + alignment.x + cohesion.x - separation.x;
        var velocityY = agent.velocity.y + alignment.y + cohesion.y - separation.y;
        var velocity = new Vector2(velocityX, velocityY).normalized * fishSpeed;

        agent.velocity = velocity;
    }

    private Vector2 getAlignment(Rigidbody2D myAgent)
    {
        var velocity = new Vector2();
        foreach(var agent in SceneController.agents)
        {
            if (agent != myAgent)
            {
                if ((myAgent.position - agent.position).sqrMagnitude < radius)
                {
                    velocity.x += agent.velocity.x;
                    velocity.y += agent.velocity.y;
                }
            }
        }
        
        velocity.Normalize();
        return velocity;
    }

    private Vector2 getCohesion(Rigidbody2D myAgent)
    {
        var velocity = new Vector2();
        int neighborsCount = 0;
        foreach (var agent in SceneController.agents)
        {
            if (agent != myAgent)
            {
                if ((myAgent.position - agent.position).sqrMagnitude < radius)
                {
                    velocity.x += agent.velocity.x;
                    velocity.y += agent.velocity.y;
                    neighborsCount++;
                }
            }
        }

        if (neighborsCount == 0)
        {
            return Vector2.zero;
        }
        
        velocity = new Vector2(velocity.x - myAgent.position.x, velocity.y - myAgent.position.y);
        velocity.Normalize();
        return velocity;
    }

    private Vector2 getSeparation(Rigidbody2D myAgent)
    {
        var velocity = new Vector2();
        foreach (var agent in SceneController.agents)
        {
            if (agent != myAgent)
            {
                if ((myAgent.position - agent.position).sqrMagnitude < radius)
                {
                    velocity.x += agent.position.x - myAgent.position.x;
                    velocity.y += agent.position.y - myAgent.position.y;
                }
            }
        }
        
        velocity.Normalize();
        return velocity;
    }
}
