﻿using UnityEngine;

public class PickUpController : MonoBehaviour {

    Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        //rb.AddTorque(15, 30, 45, ForceMode.Impulse);
	}

    void Update () {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    void FixedUpdate()
    {
        //Quaternion deltaRotation = Quaternion.Euler(new Vector3(15, 30, 45) * Time.deltaTime);
        //rb.MoveRotation(rb.rotation * deltaRotation);

        rb.AddTorque(new Vector3(15, 30, 45) * Time.deltaTime);
        
        //transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
