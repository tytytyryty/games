﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    Rigidbody rb;
    int pickedCount = 0;
    int pickUpsCount;
    string pickUpTag = "Pick Up";

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        pickUpsCount = GameObject.FindGameObjectsWithTag(pickUpTag).Length;
        setCountText();
        winText.text = "";
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        
        rb.AddForce(new Vector3(horizontal, 0, vertical) * speed);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(pickUpTag))
        {
            pickedCount++;
            other.gameObject.SetActive(false);
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + pickedCount;
        if (pickedCount == pickUpsCount)
        {
            winText.text = "You win!";
        }
    }
}
